const getPercentage = (total,req)=>{
    let totalAmt = parseInt(total)
    let find = Math.abs(parseInt(req))
    let percent = (find/total)*100
    if(totalAmt < find){
        return 100
    }
    return percent
}

// let data 
// loadData = ()=>{
// fetch('https://api.imgflip.com/get_memes')
// .then(response => response.json())
// .then(response => {
//     const {memes} = response.data;
//     data = memes
// })}
let data;
loadData = async ()=>{
    let rawData = fetch('https://api.imgflip.com/get_memes')
    data = rawData.json()
    console.log(data)
}
console.log(data)
const slideContainer = document.querySelector('.for-slides')
const slider = document.querySelectorAll('.slider')
let counter = 0;
function slide(){
    slider.forEach((slide,index) => {
        slide.style.left = `${index * 100}%`
    });
}
function slideMove(){
    slider.forEach((slide,index)=>{
        slide.style.transform = `translateX(-${counter * 100}%)`
        slide.classList.remove('active')
        if(index == counter){
            slide.classList.add('active')
        }
    })
}
const carousel = document.createElement('ul')
carousel.classList.add('slider-button-container')
const images = document.querySelectorAll('.slider img')
images.forEach((image,index)=>{
    carousel.innerHTML += `<img src=${image.src} class="slider-button">`
})
slideContainer.parentElement.append(carousel)
const carauselSelector = document.querySelectorAll('.slider-button')
prevButton = document.querySelector('.prev-button')
nextButton = document.querySelector('.next-button')
prevButton.onclick = function(){
    counter--;
    if(counter < 0){
        counter = slider.length-1
    }
    slideMove()
    classadd()
}
nextButton.onclick = function(){
    counter++;
    if(counter == slider.length){
        counter = 0
    }
    slideMove()
    classadd()
}
function classadd(){
carauselSelector.forEach((carausel,index)=>{
    counter === index ? carausel.classList.add('active') : carausel.classList.remove('active')
    carausel.addEventListener('click',(e)=>{
        console.log(e.target)
        counter = index
        slideMove()
        classadd()
    })
})
}
var dragDiv = document.querySelector('.drag-div')
var intPosition
var lastPosition
dragDiv.addEventListener('dragstart',(e)=>{
    intPosition = e.x
})
dragDiv.addEventListener('drag',(e)=>{
    e.preventDefault()
    var width = dragDiv.clientWidth
    var draggedDistance = parseInt(intPosition - e.x)
    if(draggedDistance > 0 && counter!== slider.length-1 && Math.abs(draggedDistance) < width){
        dragDiv.style.transform = `translateX(-${draggedDistance}px)`
    }
    else if(draggedDistance < 0 && counter!== 0 && Math.abs(draggedDistance) < width){
        dragDiv.style.transform = `translateX(${-draggedDistance}px)`
    }
})
dragDiv.addEventListener('dragend',(e)=>{
    e.preventDefault()
    lastPosition = e.x
    dragDiv.style.transform = `translateX(${0}px)`
    var diffrence = intPosition - lastPosition
    var diffPercent = getPercentage(dragDiv.clientWidth,diffrence)
    console.log(diffrence)
    if(diffPercent>25){
        if(diffrence>0 && counter!== slider.length-1){
            counter++
            slideMove()
            classadd()
        }
        else if(diffrence<0 && counter!== 0){
            counter--
            slideMove()
            classadd()
        }
    }
})
console.log(counter)
// setInterval(function(){
//     counter++;
//     if(counter == slider.length){
//         counter = 0
//     }
//     slideMove()
//     classadd()
//     console.log(counter)
// }, 5000)
slide()
slideMove()
classadd()