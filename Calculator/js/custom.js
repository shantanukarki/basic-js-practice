const calculator = document.querySelector('.calculator')
const keys = document.querySelector('.calculator-keys')
const display = document.querySelector('.calculator-output')
calculator.clientHeight = 0
console.log(calculator.clientLeft)
keys.addEventListener('click',e=>{
    if(e.target.matches('button')){
        const key = e.target
        var keyClass = e.target.classList.contains('is-depressed');
        if(!keyClass){
            let k = key.parentNode.children;
            for (i=0;i<k.length;i++){
            k[i].classList.remove('is-depressed')
            }
        }
        const action = key.dataset.action
        const keyContent = key.textContent
        const displayedNum = display.textContent
        const previousKeyType = calculator.dataset.previousKeyType
        const displayAction = calculator.dataset.operator
        const firstValue = calculator.dataset.firstValue
        // Array.from(key.parentNode.children).forEach(k => k.classList.remove('is-depressed'))
        if (!action) {
            if(displayedNum==0||previousKeyType === 'operator'||previousKeyType === 'output'){
                display.textContent = keyContent
                calculator.dataset.previousKeyType = ''
            }else{
                display.textContent = displayedNum +keyContent
            }
        }
        if(action==='clear'){
            display.textContent = 0
        }
        if(action === 'decimal'){
            if(previousKeyType === 'operator'){
                display.textContent = '.'
                calculator.dataset.previousKeyType = ''
            }else{
            display.textContent = displayedNum + '.'
            }
        }
        if (
            action === 'add' ||
            action === 'subtract' ||
            action === 'multiply' ||
            action === 'divide'
          ){
            display.textContent = displayedNum + key.textContent
            if(!keyClass){
                key.classList.add('is-depressed')
            }
            calculator.dataset.firstValue = displayedNum
            calculator.dataset.previousKeyType = 'operator'
            calculator.dataset.operator = action
        }
        if (action === 'calculate') {
            const secondValue = displayedNum
            display.textContent = calculate(firstValue,displayAction,secondValue)
            calculator.dataset.previousKeyType = 'output'
        }
    }
})

const calculate = (n1,operator,n2)=>{
    if (operator === 'add') {
        result = parseFloat(n1) + parseFloat(n2)
      } else if (operator === 'subtract') {
        result = parseFloat(n1) - parseFloat(n2)
      } else if (operator === 'multiply') {
        result = parseFloat(n1) * parseFloat(n2)
      } else if (operator === 'divide') {
        result = parseFloat(n1) / parseFloat(n2)
      }
      return result
}