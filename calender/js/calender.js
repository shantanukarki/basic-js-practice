const date = new Date()

var cal = document.querySelector('.calender-container')


const months = [
    "January",
    "Februrary",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
]
//add content to calender body
function calendarInit(){
    //setting the date to the first day of the month
    date.setDate(1)


    
    //last day of the curent month
    const lastDay = new Date(date.getFullYear(),date.getMonth()+1,0).getDate()
    //last day of the previous month
    const prevLastDay = new Date(date.getFullYear(),date.getMonth(),0).getDate()

    //first and last day of the current month
    const firstDayIndex =date.getDay()
    const lastDayIndex = new Date(date.getFullYear(),date.getMonth()+1,0).getDay()

    //to calculate how many days from the next month to show
    var nextDays =  7 -lastDayIndex -1;

    cal.innerHTML = ""
    
    var head = `
    <div class = "calender-date">
    <h1></h1>
    <h2></h2>
    <p></p>
    </div>
    `
    var calTop = document.createElement('div')
    calTop.classList.add('cal-top')
    calTop.innerHTML = head
    cal.prepend(calTop)
    //shows the current year month and day
    document.querySelector('.calender-date h1').innerHTML = date.getFullYear()
    document.querySelector('.calender-date h2').innerHTML = months[date.getMonth()]
    document.querySelector('.calender-date p').innerHTML = new Date().toDateString()


    var monthList = document.createElement('div');
    monthList.classList.add('cal-months');
    monthList.innerHTML = `
    <div class = "months">
    <ul>
    <li>Jan</li>
    <li>Feb</li>
    <li>Mar</li>
    <li>Apr</li>
    <li>May</li>
    <li>Jun</li>
    <li>Jul</li>
    <li>Aug</li>
    <li>Sep</li>
    <li>Oct</li>
    <li>Nov</li>
    <li>Dec</li>
    </ul>
    </div> 
    `
    cal.append(monthList)
    var calBody = document.createElement('div')
    calBody.classList.add('cal-body')
    cal.append(calBody);

    let calDays = document.createElement('div');
    calDays.classList.add('week-days')
    calDays.innerHTML = `
    <ul>
    <li>Sun</li>
    <li>Mon</li>
    <li>Tue</li>
    <li>Wed</li>
    <li>Thu</li>
    <li>Fri</li>
    <li>Sat</li>
    </ul>
    `
    calBody.prepend(calDays)
    var daysHolder = document.createElement('ul')
    daysHolder.classList.add('days-holder')
    calBody.append(daysHolder)
    let days = ""
    //shows the dates from previous month
    for(let j=firstDayIndex; j>0;j--){
        days += `<li class= "prev-date"><span>${prevLastDay - j+1}</span></li>`
        daysHolder.innerHTML = days
    }
    //shows the dates from current month
    for(let i=1;i <= lastDay;i++){
        if(i=== new Date().getDate() && date.getMonth()=== new Date().getMonth()){
            days += `<li class= "current-date today"><span>${i}</span></li>`
            daysHolder.innerHTML = days
        }else{
        days += `<li class= "current-date"> <span>${i}</span></li>`
        daysHolder.innerHTML = days
        }
    }
    //shows dates from next month
    var monthDays = document.querySelectorAll(".days-holder li")
    if(monthDays.length <= 35){
        nextDays = nextDays+7
    }
    for (let k=1;k <= nextDays; k++){
        days += `<li class = "next-date"><span>${k}</span></li>`
        daysHolder.innerHTML = days
    }

    //buttons to next or prev month
    let prevButton = document.createElement('div');
    prevButton.classList.add("prev-button");
    prevButton.innerHTML = `<i class="fas fa-chevron-left"></i>`
    calTop.prepend(prevButton)
    let nextButton = document.createElement('div');
    nextButton.classList.add("next-button");
    nextButton.innerHTML = `<i class="fas fa-chevron-right"></i>`
    calTop.append(nextButton)
    prevButton.onclick = function(){
        date.setMonth(date.getMonth() - 1)
        calendarInit()
        if(date.getMonth() != new Date().getMonth()){
            document.querySelector('.days-holder li.current-date').classList.add('today')
            document.querySelector('.calender-date p').innerHTML = ""
            document.querySelector('.calender-date p').innerHTML =  date.toDateString()
        }
        // for(let i=0;i < eventArray.length;i++){
        //     eventArray[i].Validate(date)
        // }
        Validate()
    };
    nextButton.onclick = function(){
        date.setMonth(date.getMonth() + 1)
        calendarInit()
        if(date.getMonth() != new Date().getMonth()){
            document.querySelector('.days-holder li.current-date').classList.add('today')
            document.querySelector('.calender-date p').innerHTML = ""
            document.querySelector('.calender-date p').innerHTML =  date.toDateString()
        }
        // for(let i=0;i < eventArray.length;i++){
        //     eventArray[i].Validate(date)
        // }
        Validate()
    };  
    var c = document.querySelectorAll(".days-holder li.current-date span")
    for(let i=0;i< c.length;i++){
        c[i].onclick = function(){
            var click = document.querySelectorAll(".days-holder li.current-date")
            for(j=0;j<click.length;j++){
                if(click[j].className == 'current-date today'){
                    click[j].classList.remove('today')
                }
            }
            this.parentElement.classList.add('today')
            date.setDate(this.innerHTML)
            document.querySelector('.calender-date p').innerHTML = ""
            document.querySelector('.calender-date p').innerHTML =  date.toDateString()
            // for(i=0;i < eventArray.length;i++){
            //     eventArray[i].Validate(date)
            // }
            Validate();
            for(let i = 0; i < Events.length; i++){
                if(Events[i].eYear == date.getFullYear()){
                    if((Events[i].eMonth)-1 == date.getMonth()){
                        (new Validate()).getDateOnclick()
                    }
                }
            }
        }
    }
    
    var monthList = document.querySelectorAll('.months ul li')
    for(let i=0; i < monthList.length;i++){
        if(i == date.getMonth()){
            monthList[i].classList.add('this-month')
        }
        monthList[i].onclick = function(){
            for(let j=0;j<monthList.length;j++){
                if(monthList[j] === this){
                    date.setMonth(j);
                }
            }
            calendarInit()
            if(date.getMonth() != new Date().getMonth()){
                document.querySelector('.days-holder li.current-date').classList.add('today')
                document.querySelector('.calender-date p').innerHTML = ""
                document.querySelector('.calender-date p').innerHTML =  date.toDateString()
            }
            // for(let i=0;i < eventArray.length;i++){
            //     eventArray[i].Validate()
            // }
            Validate()
        }
    }

    
}
calendarInit()

var eventContainer = document.querySelector('.event-container')
var dateContainer = document.createElement('div');
dateContainer.classList.add('date-container');
eventContainer.append(dateContainer)
var nameContainer = document.createElement('div');
nameContainer.classList.add('name-container')
eventContainer.append(nameContainer)

// class Events{
//     constructor(name,eventDate){
//         this.name = name;
//         this.date = eventDate
//     }
//     Validate(date){
//         let eDate = new Date(this.date)
//         let eName = this.name
//         date.setHours(0)
//         date.setMinutes(0)
//         date.setSeconds(0)
//         date.setMilliseconds(0)
//         if(eDate-date == 0){
//             nameContainer.innerHTML = ""
//             dateContainer.innerHTML = ""
//             console.log(this.name)
//             dateContainer.innerHTML = `<span>${date.getDate()}</span><span>${(months[date.getMonth()])}</span>`
//             nameContainer.innerHTML = `<span>${eName}</span>`
//         }
//         var calDays = document.querySelectorAll('.days-holder li.current-date')
//         for(let i=0;i < calDays.length;i++){
//             if(eDate.getMonth() == date.getMonth()){
//             var checkDate = date;
//             checkDate.setDate(calDays[i].childNodes[0].innerHTML)
//             if(checkDate-eDate == 0 ){
//                 calDays[i].classList.add('has-event')
//             }
//         }
//         }
//     }
// }

// var eventArray = []
// function arrayPush(){
//     let name = 'My Birthday';
//     let eventDate = 'sep 28 2021'
//     eventArray.push(new Events(name,eventDate))

//     name = 'Hello There';
//     eventDate = 'Oct 28 2021'
//     eventArray.push(new Events(name,eventDate))
// }


// function arrayValidate(){
//     for(let i = 0; i < eventArray.length;i++){
//         eventArray[i].Validate(date)
//         console.log(eventArray[i])
//     }
// }
// arrayPush()
// arrayValidate()

var Events =  [
    {
        eName :"Hello There",
        eYear : 2021,
        eMonth : 8,
        eDay : 28,
        link : ""
    },
    {
        eName :"Euro Cup",
        eYear : 2021,
        eMonth : 6,
        eDay : 11,
        link : "https://www.uefa.com/uefaeuro-2020/news/0254-0d41684d1216-06773df7faed-1000--euro-2020-all-the-fixtures/"
    },
    {
        eName :"Euro Final",
        eYear : 2021,
        eMonth : 7,
        eDay : 11,
        link : "https://www.uefa.com/uefaeuro-2020/news/0254-0d41684d1216-06773df7faed-1000--euro-2020-all-the-fixtures/"
    }
]
function Validate(){
    for(let i = 0 ;i < Events.length; i++){
        dateContainer.innerHTML = `<span>${date.getDate()}</span> ${" "} <span>${(months[date.getMonth()])}</span>`
        nameContainer.innerHTML = `<span>There is no event planned for ${dateContainer.innerHTML}</span>`
        window.onload = function(){
            dateContainer.innerHTML = `<span>${new Date().getDate()}</span> ${" "}  <span>${(months[date.getMonth()])}</span>`
            nameContainer.innerHTML = `<span>There is no event planned for ${dateContainer.innerHTML}</span>`
        }
        if(Events[i].eYear == date.getFullYear()){
            if((Events[i].eMonth)-1 == date.getMonth()){
                var currMonthDate = document.querySelectorAll('.days-holder li.current-date span')
                for(let j = 0 ; j<currMonthDate.length; j++){
                    if(j+1==Events[i].eDay){
                        currMonthDate[j].classList.add("has-event")
                    }
                }

                var todayDate = document.querySelector('.days-holder li.current-date.today span').innerHTML
                
                if(todayDate == Events[i].eDay){
                    dateContainer.innerHTML = `<span>${Events[i].eDay}</span> ${" "} <span>${(months[date.getMonth()])}</span>`
                    nameContainer.innerHTML = `<a href="${Events[i].link}">${Events[i].eName}</a>`
                }
                this.getDateOnclick = function(){   
                    if(date.getDate() == Events[i].eDay){
                        dateContainer.innerHTML = `<span>${Events[i].eDay}</span> ${" "} <span>${(months[date.getMonth()])}</span>`
                        nameContainer.innerHTML = `<a href="${Events[i].link}">${Events[i].eName}</a>`
                    }
                    else{
                        dateContainer.innerHTML = `<span>${date.getDate()}</span>${" "}<span>${(months[date.getMonth()])}</span>`
                        nameContainer.innerHTML = `<span>There is no event planned for ${dateContainer.innerHTML}</span>`
                    }
                }
            }
        }
    }
}
Validate()